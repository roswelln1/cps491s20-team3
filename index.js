'use strict';
 
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');


admin.initializeApp({
	credential: admin.credential.applicationDefault(),
  	databaseURL: 'https://novobi.firebaseio.com/'
});
 
process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }
  
  function handleSearchProduct(agent) {
    return admin.database().ref('products').once('value').then((snapshot) => {
      	const prod = agent.parameters.product;
    	const stock = snapshot.child(prod).child('stock_amount').val();
      	const descr = snapshot.child(prod).child('description').val();
      	const colors = snapshot.child(prod).child('colors_available').val();
      	const photo = snapshot.child(prod).child('photo').val();
    	const price = snapshot.child(prod).child('price').val();
     	const sizes = snapshot.child(prod).child('size').val();
      	const similar = snapshot.child(prod).child('similar_product').val();
      	const revCnt = snapshot.child(prod).child('review_count').val();
      	const review_text = snapshot.child(prod).child('reviews').child(revCnt).child('text').child(0).val();
      	const review_stars = snapshot.child(prod).child('reviews').child(revCnt).child('total_stars').child(0).val();
      
  	if (stock != null){
      agent.add(`There are ${stock} ${prod}s in stock at ${price}. ${prod} is available in these colors: ${colors} and these sizes: ${sizes}.\n`);
      agent.add(new Card({
                    title: `${prod}`,
        			imageUrl: [photo],
        			text: `${descr}`
                 	}));
      agent.add(new Suggestion(`Leave a review`));
      if(similar != null){
      	agent.add(new Suggestion(similar));
      }  
      if(revCnt != null){
       	agent.add(`The most recent review is: ${review_text} and the user gave it ${review_stars} out of 5 stars.\n From here you can either leave a review or request similar product information, just type what you would like to do!`);
      }
    }
      
      	else{
         	agent.add(`Data could not be found, try again`); 
        }
      
    });
  }
  
  
  function handleReview(){
      
    const prod = agent.parameters.product;
    	const text = agent.parameters.review;
      	const stars = agent.parameters.stars;
  	return admin.database().ref('products').once('value').then((snapshot) => {
      const revCnt = snapshot.child(prod).child('review_count').val();
      const nextVal = revCnt+1;
  	if (text != null && prod !=null && stars != null){
       addNewRev(prod, nextVal, revCnt);
      handleRevCnt(prod, nextVal, revCnt);
      agent.add(`Your review will be added!`);
      
      return admin.database().ref('products').child(prod).child('reviews').child(revCnt+1).set({
        	text: [text],
        	total_stars: [stars]
       });
       
    }
      
      	else{
         	agent.add(`Data could not be found, try again`); 
        }
    });
      

  }
  
  
 
  
  function handleRecommendations(){
     return admin.database().ref('products').once('value').then((snapshot) => {
      	const prod = agent.parameters.product;
    	const similar = snapshot.child(prod).child('similar_product').val();
       agent.add(`Similar products to ${prod} include: ${similar}`);
     });
  }
  
  function handlePurchase(){
  }

  function addNewRev(prod, nextVal,revCnt){
    
  	return admin.database().ref('products').child(prod).child('reviews').update({
         	[nextVal]: null
    });
  }
  
  function handleRevCnt(prod, nextVal, revCnt){
    return admin.database().ref('products').child(prod).update({
         	review_count: nextVal
    });
  }
  
  
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('search product', handleSearchProduct);
  intentMap.set('Leave product review', handleReview);
  intentMap.set('Product recommendation', handleRecommendations);
  intentMap.set('Purchase item', handlePurchase);
  agent.handleRequest(intentMap);
});